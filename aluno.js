"use strict";
exports.__esModule = true;
var Aluno = /** @class */ (function () {
    function Aluno() {
    }
    Aluno.prototype.setNome = function (nome) {
        this.nome = nome;
    };
    Aluno.prototype.setCpf = function (cpf) {
        this.cpf = cpf;
    };
    Aluno.prototype.setMatricula = function (matricula) {
        this.matricula = matricula;
    };
    return Aluno;
}());
exports.Aluno = Aluno;
var aluno1 = new Aluno();
aluno1.setNome("Jurasse");
aluno1.setCpf("123456789");
aluno1.setMatricula(171);
