export class Aluno{
    private nome:string;
    private cpf:string;
    private matricula: number;
    
    public setNome(nome:string){
        this.nome = nome;
    }
    public setCpf(cpf:string){
        this.cpf = cpf;
    }
    public setMatricula(matricula:number){
        this.matricula = matricula;
    } 
}
let aluno1 = new Aluno();

aluno1.setNome ("Jurasse");
aluno1.setCpf("123456789");
aluno1.setMatricula(171);